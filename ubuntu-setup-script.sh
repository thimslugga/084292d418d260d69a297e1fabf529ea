#!/bin/bash

apt update && apt upgrade

apt install bash bash-completion fish xonsh vim-nox busybox htop screen tmux mosh dtrx pigz pwgen curl wget \
    rsync zsync aria2 httpie tree

apt install telnet socat dnsutils whois traceroute tcptraceroute mtr-tiny dublin-traceroute tcpdump nmap \
    sipcalc 

apt install strace build-essential git python-dev python3-dev cython cython3 sqlite sqlite3 ruby nodejs \
    npm jq gnupg colordiff multitail
    
apt install mpd weechat weechat-curses weechat-scripts weechat-plugins scrot
    
apt install earlyoom openssh-server chrony ufw tlp